const path = require('path');


module.exports = {
	resolve: {
		modules: [
			path.resolve('./lib'),
			path.resolve('./node_modules'),
		],
	},
	entry: [
		'@babel/polyfill',
		'./lib/renderers/dom.js'
	],
	output: {
		path: path.resolve(__dirname, 'public'),
		filename: 'app.min.js'
	},
	module: {
		rules: [
			{ test: /.jsx?$/, exclude: /^(node_modules|__test__)$/, loaders: 'babel-loader' },
			{ test: /.css$/, loader: 'style-loader!css-loader' },
		]
	},
};
