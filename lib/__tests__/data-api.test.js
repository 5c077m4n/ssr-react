import DataApi from '../data-api';
import { data } from '../test-data.json';


const api = new DataApi(data);

describe('DataApi', () => {
	it('should test sanity', () => {
		expect(data).toBeTruthy();
		expect(api).toBeTruthy();
	});
	it('should expose the articles array as an object', () => {
		const articles = api.getArticles();
		const articleId = data.articles[0].id;
		const articleTitle = data.articles[0].title;

		expect(articles).toHaveProperty(articleId);
		expect(articles[articleId].title).toBe(articleTitle);
	});
	it('should expose the authors array as an object', () => {
		const authors = api.getAurthors();
		const authorId = data.authors[0].id;
		const authorFirstName = data.authors[0].firstName;

		expect(authors).toHaveProperty(authorId);
		expect(authors[authorId].firstName).toBe(authorFirstName);
	});
});
