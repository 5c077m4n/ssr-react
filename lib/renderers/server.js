import React from 'react';
import ReactDOMServer from 'react-dom/server';

import App from 'components/app';


const renderServer = (route = '/') => {
	switch (route) {
		case '/':
			return ReactDOMServer.renderToString(<App />);
		default:
			return '';
	}
};

export default renderServer;
