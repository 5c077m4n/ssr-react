import React from 'react';
import ReactDOM from 'react-dom';
if (!Function.name) reuqire('../function-name-polyfill');
if (!window.fetch) require('whatwg-fetch');


import App from 'components/app';


ReactDOM.hydrate(
	<App />,
	document.getElementById('root')
);
