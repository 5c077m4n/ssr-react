class DataApi {
	constructor(rawData) {
		this.rawData = rawData;
	}

	mapArrayToObject(arr) {
		return arr
			.reduce((acc, curr) => {
				acc[curr.id] = curr;
				return acc;
			}, {});
	}
	getArticles() {
		return this.mapArrayToObject(this.rawData.articles);
	}
	getAurthors() {
		return this.mapArrayToObject(this.rawData.authors);
	}
}

export default DataApi;
