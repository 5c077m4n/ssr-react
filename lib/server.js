import express from 'express';

import serverRender from './renderers/server';
import config from './config';
import { data } from './test-data.json';


const app = express();

app.use(express.static('public'));

app.set('view engine', 'ejs');
app.route('/')
	.get((req, res, next) => {
		const initialContent = serverRender();
		return res.render('index', { initialContent });
	});
app.route('/data')
	.get((req, res, next) => res.json(data));
app.listen(config.port, () => console.info(`Now listening on port ${config.port}...`));
