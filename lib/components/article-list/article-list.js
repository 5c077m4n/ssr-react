import React from 'react';

import ArticleItem from '../article-item/article-item';
import './article-list.css';


const ArticleList = props => (
	<ul>
		{Object.values(props.articles).map(article => (
			<ArticleItem
				key={article.id}
				article={article}
				articleActions={props.articleActions}
			/>
		))}
	</ul>
);

export default ArticleList;
