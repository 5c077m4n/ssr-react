import React, { Component } from 'react';

import ArticleList from './article-list/article-list';
import ErrorBoundary from './error-boundary/error-boundary';
import DataApi from '../data-api';


class App extends Component {
	state = { articles: {}, authors: {} };
	constructor() {
		super();
	}
	articleActions = { lookupAuthor: authorId => this.state.authors[authorId] };
	async componentDidMount() {
		await fetch('/data')
			.then(resp => resp.json())
			.then(data => new DataApi(data))
			.then(api => {
				return this.setState(prevState => ({
					articles: api.getArticles(),
					authors: api.getAurthors(),
				}));
			});
	}

	render() {
		return (
			<ErrorBoundary>
				<ArticleList
					articles={this.state.articles}
					authors={this.state.authors}
					articleActions={this.articleActions}
				/>
			</ErrorBoundary>
		);
	}
}

export default App;
