import React, { Component } from 'react';

import './error-boundary.css';


class ErrorBoundary extends Component {
	constructor(props) {
		super(props);
		this.state = { hasError: false };
	}

	static getDerivedStateFromError(error) {
		return { hasError: true, errorContent: error };
	}
	componentDidCatch(error, info) {
		console.error(error, info);
	}

	render() {
		if (!this.state.hasError) return this.props.children;
		else return (<p>An error has occured: {this.state.errorContent}</p>);
	}
}

export default ErrorBoundary;
