import React from 'react';
import renderer from 'react-test-renderer';

import ArticleList from '../article-list/article-list';


const testProps = {
	articles: {
		a: { id: 'a' },
		b: { id: 'b' },
	},
	articleActions: {
		lookupAuthor: jest.fn(() => ({})),
	}
};

describe('Article List Component', () => {
	it('should render correctly', () => {
		const tree = renderer
			.create(<ArticleList {...testProps} />)
			.toJSON();
		expect(tree.children.length)
			.toBe(Object.keys(testProps.articles).length);
		expect(tree)
			.toMatchSnapshot();
	});
});
