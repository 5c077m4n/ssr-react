import React from 'react';

import './article-item.css';


const dateDisplay = date => new Date(date).toDateString();

const ArticleItem = props => {
	const { article, articleActions } = props;
	const author = articleActions.lookupAuthor(article.authorId);

	return (
		<li>
			<article>
				<header><h3>{article.title}</h3></header>
				<time>{dateDisplay(article.date)}</time>
				<p>
					<a href={author.website}>{author.firstName} {author.lastName}</a>
				</p>
				<section>
					<p>{article.body}</p>
				</section>
			</article>
		</li>
	);
};

export default ArticleItem;
